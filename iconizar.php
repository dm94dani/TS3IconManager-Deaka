<?php
/*
*************************************************
/				Autor: Pedro Arenas (Doc)		/
/				Archivo: iconizar.php			/
*************************************************
*/
include './data/config.php';
include './lang/loadlang.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php echo $NAME_TITLE ?> | TS3 Iconos </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link rel="stylesheet" type="text/css" href="css/main.css"/>
</head>

<body>
<br/><br/><br/>
<div class="container">
	<div class="row">
		<center>
		<div class="panel panel-primary" style="width: 300px;">
			<div class="panel-heading">
				<h3 class="panel-title" style="height: 16px;">$lang['i_title']</h3>
			</div>
			<div class="panel-body" style="width: 300px;">
				<?php include './modulos/iconizador.php'; //Importamos el codigo a usar ?>
			</div>
		</div>
		</center>
	</div>
</div>
<div class="row">
		<div class="panel panel-primary">
			<div class="panel-body" >
				<p class="text-capitalize">Script hecho por <a href="http://twitter.com/MrDoc94">Doc</a> | SourceCode en <a href="https://github.com/Doc94/TS3IconManager">GitHub</a></p>

			</div>
		</div>
</div>
</body>
</html>
