<?php
/*
*************************************************
/		Autor: Pedro Arenas (Doc)				/
/		Modificacion: Daniel Martin (Dm94)		/
/		Archivo: index.php						/
*************************************************
*/
include './data/config.php';
include './lang/loadlang.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php echo $NAME_TITLE ?> | Deaka Club </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link rel="stylesheet" type="text/css" href="css/main.css"/>
</head>

<body>
	<script>
		var num1, num2, resul, suma;
		var palabra;
		var captcha;
		captcha=Math.floor(Math.random()*2);
		if(captcha==1){
			num1=Math.floor(25 * Math.random() + 2);
			num2=Math.floor(25 * Math.random() + 2);
			suma=num1+num2;
			resul=parseInt(prompt("Para combrobar que no eres un bot. \nTeclea el resultado de: "+num1+" + "+num2+" "));
			if(resul!=suma){
				window.location.reload();
			}
		}
		else{
			palabra=prompt("Para combrobar que no eres un bot. \nEscribe deaka ");
			if(palabra!="deaka"){
				window.location.reload();
			}
		}
	</script>
	<div class="container">
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $lang['i_title'];?></h3>
				</div>
				<div class="panel-body" >
					<form role="form" name="formulario"method="POST"action="procesar.php">
						<?php echo $lang['i_inputuid'];?><br/><br/>
						<div class="form-group">
							<input type="text" id="uniid" name="uniid" class="form-control" placeholder="UID">
						</div>
						<button type="submit" class="btn btn-default"><?php echo $lang['i_buttonsend'];?></button>
						<br/>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-body" >
				<p>Si quereís saber vuestro UID o Unique ID mirad este tutorial: <a href=""> Ver Video </a></p>
			</div>
		</div>
	</div>
</body>
</html>
